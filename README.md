<<<<<<< README.md
***Présentation de 6 commandes Git !***

**1 - git checkout**

Permet de basculer entre les branches du dépôt. Elle peut aussi être utilisée pour créer une nouvelle branche et basculer dessus directement avec l'option -b.

*Ca peut servir quand :* Pour travailler sur différentes parties d'un projet avec d'autres personnes en même temp, sans affecter la branche principale.

**2 - git status**

Ca montre les fichiers modifiés, les fichiers validé pour le commit, et les fichiers qu'on a déciddé de laisser de coté de la branche courante.

*Ca peut servir quand :* Pour organiser ses commits de manière claire.

**3 - git pull**

Récupère les modifications d'une branche sur le serveyr et les fusionne avec notre branche local .

*Ca peut servir quand :* Pour s'assurer que notre branche locale est à jour avec les dernières modifications. avant de faire des modifications sur un projet par exemple.

**4 - git add**

Ajoute les dernieres modifications des fichiers au prochain commit. 

*Ca peut servir quand :* Lorsqu'on le veut sélectionné des fichiers spécifique pour notre commit.

**5 - git commit**

Enregistre les modifications ajouté avec add sur le serveur. Possibilité d'ajouter un message décrivant le but des modifications avec l'attribut "-m".

*Ca peut servir quand :* Proposer son code aux autres membres du projet.

**6 - git log** 

Affiche l'historique des commits de la branche courante avec les messages.

*Ca peut servir quand :* Pour trouver des points spécifiques dans l'historique pour une révision par exemple.

Partie 2 :

Les conflits de fusion surviennent quand deux branches ont des modifications dans les mêmes lignes d'un fichier ou quand le fichier a été modifié dans une branche et supprimé dans une autre.

Comment résoudre les merge conflicts

Git nous informe des qu'un conflit de fusion est survenu après une tentative de pull. Dans gitlab identifiez les endroits en conflits, généralement surligner en rouge. Enfin modifiez le fichier pour intégrer les modifications souhaitées. Après avoir résolu tous les conflits il faut terminer avec git commit pour créer un nouveau commit qui intègre les modifications fusionnées.
